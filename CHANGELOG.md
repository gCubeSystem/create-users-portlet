
# Changelog for create-users-portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v3.1.0] - 2024-10-29

- Updated storage hub dependency
- Removed social-networking-library
- maven-parent 1.2.0
- maven-portal-bom 4.0.0
- #27999 StorageHub downstream components to upgrade in order to work with storagehub 1.5.0

## [v3.0.2] - 2022-06-08

- Released for HL removal in portal

## [v3.0.1] - 2021-07-08

- Feature #21777, migrated to new AccessTokenProvider class
- Bug fix #21803, create-user-portlet may not add the yet created user to all the context at once

## [v3.0.0] - 2020-09-24

- Ported to git

- #19812 modified to support the new IAM Keycloak based


## [v1.0.0] - 2016-03-28

First release, for changes between 1.0 version and 3 see changelog xml in distro folder.
